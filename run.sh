#!/bin/sh
docker run -td --name tacplus \
       -v /etc/tacacs+/tac_plus.conf:/etc/tacacs+/tac_plus.conf \
       -v   /var/log/tacacs/:/var/log/tacacs/ \
       -p 49:49/tcp \
       noc/tacplus
